#!/usr/bin/env python

## (c) 2009 Laurent Coustet
# Clarisys Informatique
#
# Script to resize a qcow2 qemu image

import struct
import decimal

QCOW2_OFLAG_COPIED = (1 << 63)
QCOW2_OFLAG_COMPRESSED = (1 << 62)

def _align_offset(offset, align):
    offset = (offset + align - 1) & ~(align - 1)
    return offset


class Qcow2Header(object):
    def __init__(self, *args):
        self.qcow2header = struct.Struct(">IIQIIQIIQQIIQ") # Big Endian
        self.dct = {}
    
    def headersize(self):
        return self.qcow2header.size

    def put(self, data):
        args = self.qcow2header.unpack(data)
        self.dct['magic'] = self.magic = args[0]
        self.dct['version'] = self.version = args[1]
        self.dct['backing_file_offset'] = self.backing_file_offset = args[2]
        self.dct['backing_file_size'] = self.backing_file_size = args[3]
        self.dct['cluster_bits'] = self.cluster_bits = args[4]
        self.dct['size'] = self.size = args[5]
        self.dct['crypt_method'] = self.crypt_method = args[6]
        self.dct['l1_size'] = self.l1_size = args[7]
        self.dct['l1_table_offset'] = self.l1_table_offset = args[8]
        self.dct['refcount_table_offset'] = self.refcount_table_offset = args[9]
        self.dct['refcount_table_clusters'] = self.refcount_table_clusters = args[10]
        self.dct['nb_snapshots'] = self.nb_snapshots = args[11]
        self.dct['snapshots_offset'] = self.snapshots_offset = args[12]

    def check(self):
        'Q' 'F' 'I' '\xfb'
        return bool((ord('Q') << 24 | ord('F') << 16 | (ord('I') << 8) | ord('\xfb') << 0) == self.magic)

    def printheader(self):
        print "magic=%s" % self.magic
        print "version=%s" % self.version
        print "backing_file_offset=%s" % self.backing_file_offset
        print "backing_file_size=%s" % self.backing_file_size
        print "cluster_bits=%s" % self.cluster_bits
        print "size=%s" % self.size
        print "crypt_method=%s" % self.crypt_method
        print "l1_size=%s" % self.l1_size
        print "l1_table_offset=%s" % self.l1_table_offset
        print "refcount_table_offset=%s" % self.refcount_table_offset
        print "refcount_table_clusters=%s" % self.refcount_table_clusters
        print "nb_snapshots=%s" % self.nb_snapshots
        print "snapshots_offset=%s" % self.snapshots_offset


    def get(self):
        return qcow2header.pack(self.magic, 
                                self.version, 
                                self.backing_file_offset, 
                                self.backing_file_size, 
                                self.cluster_bits, 
                                self.size, self.crypt_method, 
                                self.l1_size, self.l1_table_offset, 
                                self.refcount_table_offset, 
                                self.refcount_table_clusters, 
                                self.nb_snapshots, 
                                self.snapshots_offset)


class Qcow2SnapshotHeader(object):
    def __init__(self, *args):
        self.qcow2snapshotheader = struct.Struct(">QIHHIIQII") # Big Endian
        self.dct = {}

    def headersize(self):
        return self.qcow2snapshotheader.size

    def put(self, data):
        args = self.qcow2snapshotheader.unpack(data)
        self.dct['l1_table_offset'] = self.l1_table_offset = args[0]
        self.dct['l1_size'] = self.l1_size = args[1]
        self.dct['id_str_size'] = self.id_str_size = args[2]
        self.dct['name_size'] = self.name_size = args[3]
        self.dct['date_sec'] = self.date_sec = args[4]
        self.dct['date_nsec'] = self.date_nsec = args[5]
        self.dct['vm_clock_nsec'] = self.vm_clock_nsec = args[6]
        self.dct['vm_state_size'] = self.vm_state_size = args[7]
        self.dct['extra_data_size'] = self.extra_data_size = args[8]

    def printheader(self):
        print "l1_table_offset=%s" % self.l1_table_offset
        print "l1_size=%s" % self.l1_size
        print "id_str_size=%s" % self.id_str_size
        print "name_size=%s" % self.name_size
        print "date_sec=%s" % self.date_sec
        print "date_nsec=%s" % self.date_nsec
        print "vm_clock_nsec=%s" % self.vm_clock_nsec
        print "vm_state_size=%s" % self.vm_state_size
        print "extra_data_size=%s" % self.extra_data_size


    def get(self):
        return qcow2snapshotheader.pack(self.l1_table_offset,
                                        self.l1_size,
                                        self.id_str_size,
                                        self.name_size,
                                        self.date_sec,
                                        self.date_nsec,
                                        self.vm_clock_nsec,
                                        self.vm_state_size,
                                        self.extra_data_size)


def help():
    print """Qcow2 Image resizer (grow only)
            (c) 2009 Laurent Coustet <address@hidden>

            Usage: %s file 
            """

if __name__ == "__main__":
    import sys
    if len(sys.argv) < 2:
        help()
        sys.exit(0)

    header = Qcow2Header()
    f = open(sys.argv[1], "rb")
    data = f.read(header.headersize())

    if len(data) < header.headersize():
        print "Invalid header. Not a qcow2 file. Must be %s long." % (header.headersize(),)

    # Try to use unpack
    header.put(data)
    if not header.check():
        print "This is not a Qcow2 image: magic not found."
        sys.exit(0)
    #print header
    header.printheader()


    print "\n\nshow refcount_table\n"
    print "refcount_table_clusters=%s" % header.refcount_table_clusters


    if header.nb_snapshots>0:
        print "\n\nshow snapshots:\n"
    else:
        print "\n\nno snapshots\n"

    offset = header.snapshots_offset
    for i in range(header.nb_snapshots):
        print "\n"
        print "seek====%s" % offset
        f.seek(offset, 0)
        spheader = Qcow2SnapshotHeader()
        data = f.read(spheader.headersize())
        spheader.put(data)
        spheader.printheader()
        offset += (spheader.headersize() + spheader.extra_data_size)
        print "seek====%s" % offset
        f.seek(offset, 0)
        idstr = f.read(spheader.id_str_size)
        offset += spheader.id_str_size
        print "seek====%s" % offset
        f.seek(offset, 0)
        name = f.read(spheader.name_size)
        offset += spheader.name_size
        print "id=%s" % idstr
        print "name=%s" % name
        offset = _align_offset(offset, 8)
        print "cluster addr table"


        addrformat = struct.Struct(">Q") # Big Endian
        max_cluster_num = header.size / (1 << header.cluster_bits)
        l2_table_cluster_num = 1 << (header.cluster_bits - 3)
        print "max_cluster_num = %s" % max_cluster_num
        cluster_index = 0
        addrmask = ~QCOW2_OFLAG_COPIED & ~QCOW2_OFLAG_COMPRESSED & ~((1 << header.cluster_bits) - 1)
        for j in range(header.l1_size):
            if cluster_index >= max_cluster_num:
                break
            f.seek(spheader.l1_table_offset + j * 8, 0)
            data = f.read(8)
            l2_table_offset = addrformat.unpack(data)[0]
            l2_table_offset = l2_table_offset & addrmask
            print "l2_table_offset=%x" % l2_table_offset
            if l2_table_offset == 0:
                cluster_index += l2_table_cluster_num
            else:
                for k in xrange(1 << (header.cluster_bits - 3)):
                    f.seek(l2_table_offset + k * 8, 0)
                    data = f.read(8)
                    cluster_offset = addrformat.unpack(data)[0]
                    if cluster_offset != 0:
                        cluster_offset = cluster_offset & addrmask
                        print "%u,0x%X" % (cluster_index, cluster_offset)
                    cluster_index += 1

    f.close()

