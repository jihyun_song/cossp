
"""Implementation of qcow2 driver."""

import os
import time
import functools
import warnings
import struct

from cinder import db
from cinder import exception
from cinder import utils
from oslo.config import cfg
from cinder.openstack.common import log as logging
from cinder.volume.island.vdisk.qcow2 import qcow2format


qcow2_opts = [
        cfg.StrOpt('qcow2_num_shell_tries',
                    default=3,
                    help='number of times to attempt to run shell commands\
                            , it is same to FLAG.num_shell_tries'),
        ]

FLAGS = cfg.CONF
FLAGS.register_opts(qcow2_opts)
LOG = logging.getLogger(__name__)


def _align_offset(offset, align):
    offset = (offset + align - 1) & ~(align - 1)
    return offset


def _try_execute(*command, **kwargs):
    #NOTE(rongze): It is same to VolumeDriver._try_execute
    tries = 0
    while True:
        try:
            utils.execute(*command, **kwargs)
            return True
        except exception.ProcessExecutionError:
            tries = tries + 1
            if tries >= FLAGS.qcow2_num_shell_tries:
                raise
            LOG.exception(_("Recovering from a failed execute. "
                            "Try number %s"), tries)
            time.sleep(tries ** 2)


def _generate_vdisk_path(context, vdisk_name):
    vdisk_path = FLAGS.local_volume_directory + vdisk_name
    return vdisk_path


def create_vdisk(context, vdisk_name, size):
    """Create a vdisk"""
    vdisk_path = _generate_vdisk_path(context, vdisk_name)
    _try_execute('qemu-img', 'create', '-f', 'qcow2', '-o',
                 'cluster_size=%s' % FLAGS.cluster_size, vdisk_path, size)


def vdisk_exist(context, vdisk_name):
    vdisk_path = _generate_vdisk_path(context, vdisk_name)
    return os.path.exists(vdisk_path)


def delete_vdisk(context, vdisk_name):
    """Delete a vdisk"""
    #TODO we don't need realy delete the vdisk, we can move it to a directory
    vdisk_path = _generate_vdisk_path(context, vdisk_name)
    os.unlink(vdisk_path)


def read_vdisk_meta(context, vdisk_name):
    """Get all metadata of a vdisk or raise if vdisk does not exist"""
    vdisk_path = _generate_vdisk_path(context, vdisk_name)
    fd = open(vdisk_path, 'rb')
    header = qcow2format.Qcow2Header()
    data = fd.read(header.headersize())
    header.put(data)
    vdisk_meta = {}
    vdisk_meta['format'] = 'qcow'
    vdisk_meta['version'] = '2'
    vdisk_meta['size'] = header.size
    vdisk_meta['cluster_size'] = 1 << header.cluster_bits
    vdisk_meta['snapshots_number'] = header.nb_snapshots
    vdisk_meta['header'] = header.dct
    vdisk_meta['snapshots'] = {}
    offset = header.snapshots_offset
    for i in range(header.nb_snapshots):
        fd.seek(offset, 0)
        snapshot_header = qcow2format.Qcow2SnapshotHeader()
        data = fd.read(snapshot_header.headersize())
        snapshot_header.put(data)
        offset += snapshot_header.headersize()
        offset += snapshot_header.extra_data_size
        offset += snapshot_header.id_str_size
        fd.seek(offset, 0)
        snapshot_name = fd.read(snapshot_header.name_size)
        snapshot_name = str(snapshot_name)
        vdisk_meta['snapshots'][snapshot_name] = snapshot_header.dct
        offset += snapshot_header.name_size
        offset = _align_offset(offset, 8)
    fd.close()
    return vdisk_meta


def get_vdisk_pointer_table(context, vdisk_name):
    return _get_pointer_table(context, vdisk_name, 'HEAD')

def get_snapshot_pointer_table(context, vdisk_name, snapshot_name):
    if not snapshot_name or snapshot_name == 'HEAD':
        raise exception.VdiskException()
    return _get_pointer_table(context, vdisk_name, snapshot_name)

def _get_pointer_table(context, vdisk_name, snapshot_name):
    vdisk_path = _generate_vdisk_path(context, vdisk_name)
    vdisk_meta = read_vdisk_meta(context, vdisk_name)
    fd = open(vdisk_path, 'rb')
    addrformat = struct.Struct(">Q")  # Big endian
    header = vdisk_meta['header']
    if snapshot_name == 'HEAD':
        spheader = header
    else:
        if vdisk_meta['header']['nb_snapshots'] == 0:
            raise exception.VdiskException()
        if snapshot_name not in vdisk_meta['snapshots']:
            raise exception.VdiskException()
        spheader = vdisk_meta['snapshots'][snapshot_name]
    max_cluster_num = header['size'] / (1 << header['cluster_bits'])
    l2_table_cluster_num = 1 << (header['cluster_bits'] - 3)
    block_index = 0
    pointer_table = {}
    addrmask = ~qcow2format.QCOW2_OFLAG_COPIED &\
               ~qcow2format.QCOW2_OFLAG_COMPRESSED &\
               ~((1 << header['cluster_bits']) - 1)
    for j in range(spheader['l1_size']):
        if block_index >= max_cluster_num:
            break
        fd.seek(spheader['l1_table_offset'] + j * 8, 0)
        data = fd.read(8)
        l2_table_offset = addrformat.unpack(data)[0]
        l2_table_offset = l2_table_offset & addrmask
        if l2_table_offset == 0:
            block_index += l2_table_cluster_num
        else:
            for k in xrange(1 << (header['cluster_bits'] - 3)):
                fd.seek(l2_table_offset + k * 8, 0)
                data = fd.read(8)
                cluster_offset = addrformat.unpack(data)[0]
                if cluster_offset != 0:
                    cluster_offset = cluster_offset & addrmask
                    pointer_table[block_index] = cluster_offset
                block_index += 1

    return pointer_table


def create_snapshot(context, vdisk_name, snapshot_name):
    """Create a snapshot for vdisk or raise if vdisk does not exist"""
    vdisk_path = _generate_vdisk_path(context, vdisk_name)
    _try_execute('qemu-img', 'snapshot', '-c', snapshot_name, vdisk_path,
                 run_as_root=True)


def delete_snapshot(context, vdisk_name, snapshot_name):
    """Delete a snapshot for vdisk or raise if vdisk does not exist"""
    vdisk_path = _generate_vdisk_path(context, vdisk_name)
    _try_execute('qemu-img', 'snapshot', '-d', snapshot_name, vdisk_path,
                 run_as_root=True)


def get_update_blocks(context, vdisk_name,
                      old_snapshot_name, new_snapshot_name):
    """Get difference blocks bettwen old snapshot and new snapshot"""
    new_table = get_snapshot_pointer_table(context,
                                           vdisk_name,
                                           new_snapshot_name)
    if not old_snapshot_name:
        return new_table

    old_table = get_snapshot_pointer_table(context,
                                           vdisk_name,
                                           old_snapshot_name)
    update_table = {}
    for block_index in new_table:
        if (block_index not in old_table) or\
                (new_table[block_index] != old_table[block_index]):
            update_table[block_index] = new_table[block_index]
    return update_table


def _get_free_cluster(context, vdisk_name):
    """Return iter of free cluster
    NOTE(rongze): This is only get free cluster from first refcount_block
    if cluster_size is 2M, a refcount_block only have 2^20 clusters.
    The image have < 2TB capacity.
    """
    vdisk_path = _generate_vdisk_path(context, vdisk_name)
    meta = read_vdisk_meta(context, vdisk_name)
    refcount_table_offset = meta['header']['refcount_table_offset']
    ref_fmt = struct.Struct(">H")  # Big Endian
    addr_fmt = struct.Struct(">Q")

    fd = open(vdisk_path, 'rb')
    fd.seek(refcount_table_offset, 0)
    data = fd.read(8)  # Only read first refcount_block_offset
    refcount_block_offset = addr_fmt.unpack(data)[0]
    fd.seek(refcount_block_offset, 0)
    data = fd.read(FLAGS.cluster_size)
    fd.close()

    def ff(ref_block_data, offset):
        for i in xrange(len(ref_block_data) / 2):
            refcount = ref_fmt.unpack(ref_block_data[i * 2:(i + 1) * 2])[0]
            if refcount == 0:
                yield i, offset + i * 2
    
    return ff(data, refcount_block_offset)


def read_vdisk(context, vdisk_name, blocks):
    """read multi block data

    paramter: blocks is a dict 'block_index => offset'
    return a iter

    """
    vdisk_path = _generate_vdisk_path(context, vdisk_name)
    fd = open(vdisk_path, 'rb')
    for block_index in blocks:
        fd.seek(blocks[block_index], 0)
        data = fd.read(FLAGS.cluster_size)
        yield block_index, data
    fd.close()


def write_vdisk(context, vdisk_name, multi_block_iter):
    """write multi block data to new vdisk

    NOTE: The max capacity is 2000GB when cluster_size == 2M
    NOTE: It is only used for new vdisk.
    paramter: muti_block_iter  is  a iter , return block_index, block_data

    """

    vdisk_path = _generate_vdisk_path(context, vdisk_name)
    meta = read_vdisk_meta(context, vdisk_name)
    ref_fmt = struct.Struct(">H")  # Big Endian
    addr_fmt = struct.Struct(">Q")
    addrmask = ~qcow2format.QCOW2_OFLAG_COPIED &\
               ~qcow2format.QCOW2_OFLAG_COMPRESSED &\
               ~((1 << meta['header']['cluster_bits']) - 1)

    free_cluster_iter = _get_free_cluster(context, vdisk_name)
    l1_table_offset = meta['header']['l1_table_offset']
    fd = open(vdisk_path, 'rb+')  # open must after _get_free_cluster
    l2_table_size = FLAGS.cluster_size / 8
    for (block_index, block_data) in multi_block_iter:
        # Find L2 table offset in L1 table
        l1_table_index = block_index / l2_table_size
        fd.seek(l1_table_offset + l1_table_index * 8, 0)
        data = fd.read(8)
        l2_table_offset = addr_fmt.unpack(data)[0]
        l2_table_offset &= addrmask
        if l2_table_offset == 0:
            # Alloc L2 table
            # Find free cluster
            cluster_index, offset = free_cluster_iter.next()
            # Update cluster refcount = 1
            ref_data = ref_fmt.pack(1)
            fd.seek(offset, 0)
            fd.write(ref_data)
            # memset 0 to L2 table
            data = bytearray(FLAGS.cluster_size)  # 0
            fd.seek(cluster_index * FLAGS.cluster_size, 0)
            fd.write(data)
            # Update L1 table
            address = cluster_index * FLAGS.cluster_size
            address |= qcow2format.QCOW2_OFLAG_COPIED  # important
            data = addr_fmt.pack(address)
            fd.seek(l1_table_offset + l1_table_index * 8, 0)
            fd.write(data)
            l2_table_offset = cluster_index * FLAGS.cluster_size

        # Finde cluster offset in L2 table
        l2_table_index = block_index % l2_table_size
        fd.seek(l2_table_offset + l2_table_index * 8, 0)
        data = fd.read(8)
        cluster_offset = addr_fmt.unpack(data)[0]
        cluster_offset &= addrmask
        if cluster_offset == 0:
            # Alloc cluster
            cluster_index, offset = free_cluster_iter.next()
            # Update cluster refcount = 1
            ref_data = ref_fmt.pack(1)
            fd.seek(offset, 0)
            fd.write(ref_data)
            # Update L2 table
            address = cluster_index * FLAGS.cluster_size
            address |= qcow2format.QCOW2_OFLAG_COPIED  # important
            data = addr_fmt.pack(address)
            fd.seek(l2_table_offset + l2_table_index * 8, 0)
            fd.write(data)
            cluster_offset = cluster_index * FLAGS.cluster_size
        
        # Write block data to cluster
        fd.seek(cluster_offset, 0)
        fd.write(block_data)
    
    fd.flush()
    fd.close()
