
""" Defines interface for vdisk access.

"""


from cinder import exception
from oslo.config import cfg

from cinder import utils

vdisk_opts = [
        cfg.StrOpt('vdisk_driver',
                    default='qcow2',
                    help='The driver to use for vdisk'),
        cfg.IntOpt('cluster_size',
                    default=2097152,
                    help='The cluster size of qcow2 vdisk'),
        ]

FLAGS = cfg.CONF
FLAGS.register_opts(vdisk_opts)

IMPL = utils.LazyPluggable('vdisk_driver',
                            qcow2='cinder.volume.island.vdisk.qcow2.api')

##############################

def vdisk_exist(context, vdisk_name):
    return IMPL.vdisk_exist(context, vdisk_name)

def create_vdisk(context, vdisk_name, size):
    """Create a vdisk"""
    return IMPL.create_vdisk(context, vdisk_name, size)

def delete_vdisk(context, vdisk_name):
    """Delete a vdisk"""
    return IMPL.delete_vdisk(context, vdisk_name)

def read_vdisk_meta(context, vdisk_name):
    """Get all metadata of a vdisk or raise if vdisk does not exist"""
    """return vdisk_meta
    example:
        {'format': 'qcow',
         'version': '2',
         'size': 1073741824,
         'cluster_size': 2097152,
         'snapshots_number': 4,
         'snapshots': ['snapshot-xxxxxxxxxxxxxxxxxxxxxxxxxa',
                       'snapshot-xxxxxxxxxxxxxxxxxxxxxxxxxb',
                       'snapshot-xxxxxxxxxxxxxxxxxxxxxxxxxc',
                       'snapshot-xxxxxxxxxxxxxxxxxxxxxxxxxd'
                       ]}
    """
    return IMPL.read_vdisk_meta(context, vdisk_name)


def create_snapshot(context, vdisk_name, snapshot_name):
    """Create a snapshot for vdisk or raise if vdisk does not exist"""
    return IMPL.create_snapshot(context, vdisk_name, snapshot_name)

def delete_snapshot(context, vdisk_name, snapshot_name):
    """Delete a snapshot for vdisk or raise if vdisk does not exist"""
    return IMPL.delete_snapshot(context, vdisk_name, snapshot_name)

def get_update_blocks(context, vdisk_name,
                      old_snapshot_name, new_snapshot_name):
    """Get difference blocks bettwen old snapshot and new snapshot"""
    return IMPL.get_update_blocks(context, vdisk_name,
                                  old_snapshot_name, new_snapshot_name)

def write_vdisk(context, vdisk_name, multi_block_iter):
    """write multi block data to new vdisk

    NOTE: The max capacity is 2000GB when cluster_size == 2M
    NOTE: It is only used for new vdisk.
    paramter: muti_block_iter  is  a iter , return block_index, block_data

    """
    return IMPL.write_vdisk(context, vdisk_name, multi_block_iter)

def read_vdisk(context, vdisk_name, blocks):
    """read multi block data

    paramter: blocks is a dict 'block_index => offset'
    return a iter

    """
    return IMPL.read_vdisk(context, vdisk_name, blocks)
