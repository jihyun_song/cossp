

from cinder import exception

from oslo.config import cfg

from cinder.volume.drivers.island.vdisk import api as vdisk
from cinder.volume.drivers.island.objectstorage import api as objectpool
import json


snapshot_opts = [
        cfg.StrOpt('snapshot_pt_template',
                    default='%s-pointer-table',
                    help='Template string to generate pointer table name'),
        cfg.StrOpt('snapshot_block_template',
                   default='%s-block-%s',
                   help='Template string to generate block name')
        ]

FLAGS = cfg.CONF
FLAGS.register_opts(snapshot_opts)


def put_snapshot(context, volume_name, snapshot_name, lastsnapshot_name=None):
    """Put the snapshot from vdisk to object storage"""
    if lastsnapshot_name:
        lastsnapshot_pt = _get_snapshot_pointer_table(context,
                                                      lastsnapshot_name)
    else:
        lastsnapshot_pt = {}

    vdisk_meta = vdisk.read_vdisk_meta(context, volume_name)
    if lastsnapshot_name not in vdisk_meta['snapshots']:
        lastsnapshot_name = None

    update_blocks = vdisk.get_update_blocks(context,
                                            volume_name,
                                            lastsnapshot_name,
                                            snapshot_name)
    snapshot_pt = _make_snapshot_pointer_table(context,
                                               snapshot_name,
                                               update_blocks,
                                               lastsnapshot_pt)
    _put_snapshot_pointer_table(context, snapshot_name, snapshot_pt)

    mutil_block_iter = vdisk.read_vdisk(context,
                                        volume_name,
                                        update_blocks)

    for (block_index, data) in mutil_block_iter:
        _put_snapshot_block(context, snapshot_name, block_index, data)
        _link_snapshot_block(context, snapshot_name,
                             block_index, snapshot_name)

    for block_index in snapshot_pt:
        if snapshot_pt[block_index] != snapshot_name:
            _link_snapshot_block(context, snapshot_pt[block_index],
                                 block_index, snapshot_name)


def get_snapshot(context, snapshot_name, new_volume_name):
    """Get the snapshot from  object storage to vdisk"""
    snapshot_pt = _get_snapshot_pointer_table(context, snapshot_name)

    def sim_iter(context, snapshot_pt):
        for block_index in snapshot_pt:
            data = _get_snapshot_block(context,
                                       snapshot_pt[block_index],
                                       block_index)
            yield block_index, data

    mutil_block_iter = sim_iter(context, snapshot_pt)
    vdisk.write_vdisk(context, new_volume_name, mutil_block_iter)


def delete_snapshot(context, snapshot_name):
    """Delete snapshot in object storage"""
    snapshot_pt = _get_snapshot_pointer_table(context, snapshot_name)
    for block_index in snapshot_pt:
        _unlink_snapshot_block(context, snapshot_pt[block_index],
                               block_index, snapshot_name)
    _delete_snapshot_pointer_table(context, snapshot_name)


def _make_snapshot_pointer_table(context, snapshot_name,
                                 update_blocks, old_pointer_table):
    pt = dict(old_pointer_table)
    for block_index in update_blocks:
        pt[block_index] = snapshot_name
    return pt


def _put_snapshot_pointer_table(context, snapshot_name, snapshot_pt):
    object_name = FLAGS.snapshot_pt_template % snapshot_name
    data = json.dumps(snapshot_pt)
    size = len(data)
    objectpool.put_object(context, object_name, data, size)


def _get_snapshot_pointer_table(context, snapshot_name):
    object_name = FLAGS.snapshot_pt_template % snapshot_name
    data = objectpool.get_object(context, object_name)
    pt = json.loads(data)
    new_pt = {}
    for block_index in pt:
        new_pt[int(block_index)] = pt[block_index]
    return new_pt


def _delete_snapshot_pointer_table(context, snapshot_name):
    object_name = FLAGS.snapshot_pt_template % snapshot_name
    objectpool.delete_object(context, object_name)


def _link_snapshot_block(context, src_snapshot_name,
                        block_index, snapshot_name):
    block_name = FLAGS.snapshot_block_template %\
            (src_snapshot_name, block_index)
    meta = objectpool.get_object_meta(context, block_name)
    if 'X-last-link-snapshot' not in meta:
        msg = _("Has no X-last-link-snapshot attribute")
        raise exception.InvalidObject(object_name=object_name,
                                      reason=msg)
    if 'X-refcount' not in meta:
        msg = _("Has no X-refcount attribute")
        raise exception.InvalidObject(object_name=object_name,
                                      reason=msg)

    if meta['X-last-link-snapshot'] == snapshot_name:
        # already link
        return

    refcount = int(meta['X-refcount'])
    refcount += 1
    objectpool.set_object_meta(context,
                               block_name,
                               {'X-refcount': str(refcount),
                                'X-last-link-snapshot': snapshot_name})


def _unlink_snapshot_block(context, src_snapshot_name,
                          block_index, snapshot_name):
    block_name = FLAGS.snapshot_block_template %\
            (src_snapshot_name, block_index)
    meta = objectpool.get_object_meta(context, block_name)
    if 'X-last-unlink-snapshot' not in meta:
        msg = _("Has no X-last-unlink-snapshot attribute")
        raise exception.InvalidObject(object_name=block_name,
                                      reason=msg)
    if 'X-refcount' not in meta:
        msg = _("Has no X-refcount attribute")
        raise exception.InvalidObject(object_name=block_name,
                                      reason=msg)

    if meta['X-last-unlink-snapshot'] == snapshot_name:
        # already unlink
        return

    refcount = int(meta['X-refcount'])
    refcount -= 1
    if refcount <= 0:
        _delete_snapshot_block(context, src_snapshot_name, block_index)
    else:
        objectpool.set_object_meta(context,
                                   block_name,
                                   {'X-refcount': str(refcount),
                                    'X-last-unlink-snapshot': snapshot_name})


def _put_snapshot_block(context, snapshot_name, block_index, data):
    block_name = FLAGS.snapshot_block_template %\
            (snapshot_name, block_index)
    if len(data) != FLAGS.cluster_size:
        raise exception.InvaildDataSize(
                _('data size is not eqaul cluster_size'))
    objectpool.put_object(context, block_name, data, FLAGS.cluster_size)
    objectpool.set_object_meta(context,
                               block_name,
                               {'X-refcount': '0',
                                'X-last-link-snapshot': '',
                                'X-last-unlink-snapshot': ''})


def _get_snapshot_block(context, snapshot_name, block_index):
    block_name = FLAGS.snapshot_block_template %\
            (snapshot_name, block_index)
    data = objectpool.get_object(context, block_name)
    if len(data) != FLAGS.cluster_size:
        raise exception.InvaildDataSize(
                _('data size is not eqaul cluster_size'))
    return data


def _delete_snapshot_block(context, snapshot_name, block_index):
    block_name = FLAGS.snapshot_block_template %\
            (snapshot_name, block_index)
    objectpool.delete_object(context, block_name)
