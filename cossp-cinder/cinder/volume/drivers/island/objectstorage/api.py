
""" Defines interface for ObjectStorage access.

"""


from cinder import exception
from oslo.config import cfg

from cinder import utils

object_opts = [
        cfg.StrOpt('objectstorage_backend',
                    default='ext4',
                    help='The backend to use for objectstorage'),
        ]

FLAGS = cfg.CONF
FLAGS.register_opts(object_opts)

IMPL = utils.LazyPluggable('objectstorage_backend',
                            ext4='cinder.volume.island.objectstorage.ext4.api')

##############################

def is_exist(context, object_name):
    return IMPL.is_exist(context, object_name)


def get_object_meta(context, object_name):
    """Get all metadata of a object or raise if object does not exist.
    
    :returns: a dict of (metadataname, value)
    
    example:
    {'mtime': 'Fri, 15, Jun 2012 13:34:18 GMT',
     'checksum': '6be23031dfe3109b3804640950d90eb7',
     'size': '4194304',
     'X-*': 'User-defined metadata'}

     values allways are string

    """
    return IMPL.get_object_meta(context, object_name)

def set_object_meta(context, object_name, meta):
    """Set User-defined metadata of a object or raise if object does not exist.
    
    :param meta     :key-value dict of metadata, value must is string
    """
    return IMPL.set_object_meta(context, object_name, meta)

def get_object(context, object_name):
    """Get a object data or raise if object does not exist."""
    return IMPL.get_object(context, object_name)

def put_object(context, object_name, buf, size):
    """Put a object data"""
    return IMPL.put_object(context, object_name, buf, size)

def delete_object(context, object_name):
    """Delete a object"""
    return IMPL.delete_object(context, object_name)

