
"""Implementation of EXT4 backend."""

import os
import md5
import time
import xattr
import datetime
import functools
import warnings

from cinder import db
from cinder import exception
from cinder import utils
from oslo.config import cfg

from cinder.openstack.common import log as logging


ext4_opts = [
        cfg.StrOpt('objectstorage_pool',
                    default='/var/openstack/objectpool/',
                    help='The directory to use for objectstorage'),
        cfg.StrOpt('objectstorage_directory',
                    default='snapshot_objects/',
                    help='The directory to use for objectstorage'),
        ]

FLAGS = cfg.CONF
FLAGS.register_opts(ext4_opts)
LOG = logging.getLogger(__name__)


#the same asimport  import   test_ext4.py
def _generate_object_path(context, object_name):
    project_id = context.project_id
    object_path = FLAGS.objectstorage_pool + object_name
    return object_path


def is_exist(context, object_name):
    object_path = _generate_object_path(context, object_name)
    return os.path.exists(object_path)


def get_object_meta(context, object_name):
    """Get all metadata of a object or raise if object does not exist.

    :returns: a dict of (metadataname, value)

    example:
    {'mtime': 'Fri, 15, Jun 2012 13:34:18 GMT',
     'checksum': '6be23031dfe3109b3804640950d90eb7',
     'size': '4194304',
     'X-*': 'User-defined metadata'}

    """
    meta = {}
    object_path = _generate_object_path(context, object_name)
    # TODO if No such the directory or file
    if not os.path.exists(object_path):
        raise exception.NotExistObject(object_name=object_name)
    statinfo = os.stat(object_path)
    mtime = time.localtime(statinfo.st_mtime)
    meta['mtime'] = time.strftime('%a, %d %b %Y %H:%M:%S %Z', mtime)
    meta['size'] = str(statinfo.st_size)

    all_xattrs = xattr.xattr(object_path)
    for key in all_xattrs:
        meta[key[5:]] = all_xattrs.get(key) # user.xxx => xxx

    return meta


def set_object_meta(context, object_name, meta):
    """Set metadata of a object or raise if object does not exist.
    
    :param meta     :key-value dict of metadata
    """
    object_path = _generate_object_path(context, object_name)
    # TODO if No such the directory or file
    if not os.path.exists(object_path):
        raise exception.NotExistObject(object_name=object_name)
    for key in meta:
        if key.startswith('X-'):
            xattr.setxattr(object_path, 'user.' + key, str(meta.get(key)))


def get_object(context, object_name):
    """Get a object data or raise if object does not exist."""
    object_path = _generate_object_path(context, object_name)
    # TODO if No such the directory or file
    if not os.path.exists(object_path):
        raise exception.NotExistObject(object_name=object_name)
    fp = open(object_path, 'rb')
    data = fp.read()
    return data


def put_object(context, object_name, buf, size):
    """Put a object data"""
    if len(buf) != size:
        raise Exception
    object_path = _generate_object_path(context, object_name)
    # TODO if No such the directory or file
    fp = open(object_path, 'wb')
    fp.write(buf)
    md5_str = md5.new(buf).hexdigest()
    xattr.setxattr(object_path, 'user.checksum', md5_str)


def delete_object(context, object_name):
    """Delete a object"""
    object_path = _generate_object_path(context, object_name)
    # TODO if No such the directory or file
    # TODO we need move the object to Tran directory
    if not os.path.exists(object_path):
        raise exception.NotExistObject(object_name=object_name)
    os.unlink(object_path)
