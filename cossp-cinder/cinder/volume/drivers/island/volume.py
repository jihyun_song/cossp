
"""
Drivers for volumes.

"""

import eventlet
import time
import libvirt
import libvirt_qemu

from lxml import etree
from cinder import context as ct
from cinder import exception
from oslo.config import cfg

from cinder.image import image_utils

from cinder.openstack.common import log as logging
from cinder.openstack.common import excutils
from cinder.openstack.common import rpc
from cinder import utils
from cinder.volume import driver
from cinder.volume.drivers.island.vdisk import api as vdisk
from cinder.volume.drivers.island import snapshot as snaper
from cinder import utils

LOG = logging.getLogger(__name__)
island_opts = [
    cfg.StrOpt('local_volume_directory',
               default='/opt/stack/data/cinder/volumes/',
               help="the directory where volumes reside")]

FLAGS = cfg.CONF
FLAGS.register_opts(island_opts)


class IslandDriver(driver.VolumeDriver):
    """Executes commands relating to Island Volumes"""

    def _find_pre_snapshot(self, snapshot):
        return self._find_snapshot(snapshot, 'PRE')

    def _find_next_snapshot(self, snapshot):
        return self._find_snapshot(snapshot, 'NEXT')

    def _find_snapshot(self, snapshot, direct):
        snapshots = self.db.snapshot_get_all_for_volume_order_by_created(
                ct.get_admin_context(),
                snapshot['volume_id'])
        if direct == 'PRE':
            pass
        elif direct == 'NEXT':
            snapshots.reverse()
        else:
            raise exception.Error('Unknow options')

        pre_snapshot = None
        found = False
        for ss in snapshots:
            if ss['id'] == snapshot['id']:
                found = True
                break
            else:
                pre_snapshot = ss

        if not found:
            # It cannot be happen
            raise exception.Error("can not find snapshot %s in db"
                    % snapshot['id'])

        return pre_snapshot

    def _can_create_snapshot_in_image(self, snapshot):
        pre_snapshot = self._find_pre_snapshot(snapshot)

        # the snapshot can be upload when the pre_snapshot is available
        if (not pre_snapshot) or \
            pre_snapshot['status'] in ['available', 'uploading']:
            return True
        else:
            return False

    def _can_upload_snapshot_to_objectstorage(self, snapshot):
        pre_snapshot = self._find_pre_snapshot(snapshot)

        # the snapshot can be upload when the pre_snapshot is available
        if (not pre_snapshot) or pre_snapshot['status'] == 'available':
            return True
        else:
            return False

    def _can_delete_snapshot_in_image(self, snapshot):
        return self._can_delete_snapshot(snapshot)

    def _can_delete_snapshot_in_objectstorage(self, snapshot):
        return self._can_delete_snapshot(snapshot)

    def _can_delete_snapshot(self, snapshot):
        next_snapshot = self._find_next_snapshot(snapshot)

        # the snapshot can be deleted when next_snapshot is available
        if next_snapshot and next_snapshot['status'] == 'available':
            return True
        else:
            return False

    def do_setup(self, context):
        self._context = context

    def check_for_setup_error(self):
        """Return an error if prerequisites aren't met"""
        pass

    def attach_volume(self, context, volume, instance_uuid, host_name, mountpoint):
        pass

    def detach_volume(self, context, volume_id):
        #TODO in this , volume have been detach by instance

        # Create a snapshot for detaching
        volume_ref = self.db.volume_get(self._context, volume_id)
        options = {
            'volume_id': volume_ref['id'],
            'user_id': volume_ref['user_id'],
            'project_id': volume_ref['project_id'],
            'status': "creating",
            'progress': '0%',
            'volume_size': volume_ref['size'],
            'display_name': "for_detach",
            'display_description': "for_detach"}
        snapshot_ref = self.db.snapshot_create(self._context, options)

        try:
            self.create_snapshot(snapshot_ref)
        except Exception, e:
            print e
            with excutils.save_and_reraise_exception():
                self.db.snapshot_update(self._context,
                                        snapshot_ref['id'],
                                        {'status': 'error'})
        self.db.snapshot_update(self._context,
                                snapshot_ref['id'], {'status': 'available',
                                                     'progress': '100%'})
        # delete volume image
        vdisk.delete_vdisk(context, volume_ref['name'])

    def create_volume(self, volume):
        """Create a volume(qcow2 format)"""
        pass

    def create_volume_from_snapshot(self, volume, snapshot):
        """Create a volume from a snapshot"""
        pass

    def clone_image(self, volume, image_location):
        return None, False

    def delete_volume(self, volume):
        """Deletes a volume"""
        snapshots = self.db.snapshot_get_all_for_volume_order_by_created(
                self._context,
                volume['id'])
        for snapshot in snapshots:
            if snapshot['status'] != 'available':
                raise exception.VolumeIsBusy(volume_name=volume['name'])

    def create_snapshot(self, snapshot):
        # must wait 2.5 second for db item sort
        eventlet.sleep(2.5)

        # Create snapshot in image
        while not self._can_create_snapshot_in_image(snapshot):
            eventlet.sleep(0.1)

        @utils.synchronized(snapshot['volume_id'] + '-' + 'image')
        def do_create_snapshot_in_image():
            self._create_snapshot_in_image(snapshot)
        do_create_snapshot_in_image()

        # Update snapshot status
        self.db.snapshot_update(self._context,
                                snapshot['id'],
                                {'status': 'uploading'})

        # Upload snapshot to objectstorage
        while not self._can_upload_snapshot_to_objectstorage(snapshot):
            eventlet.sleep(10)

        pre_snapshot = self._find_pre_snapshot(snapshot)
        volume_ref = self.db.volume_get(self._context, snapshot['volume_id'])
        if not pre_snapshot:
            lastsnapshot_name = None
        else:
            lastsnapshot_name = pre_snapshot['name']

        @utils.synchronized(snapshot['volume_id'] + '-' + 'objectstorage')
        def do_upload_snapshot_to_objectstorage():
            snaper.put_snapshot(snapshot,
                                volume_ref['name'],
                                snapshot['name'],
                                lastsnapshot_name)
        do_upload_snapshot_to_objectstorage()

        # Delete old snapshot in image
        pre_snapshot = self._find_pre_snapshot(snapshot)
        if pre_snapshot and pre_snapshot['status'] == 'available':
            @utils.synchronized(snapshot['volume_id'] + '-' + 'image')
            def do_delete_snapshot_in_image():
                self._delete_snapshot_in_image(pre_snapshot)
            do_delete_snapshot_in_image()

    def delete_snapshot(self, snapshot):
        # last available snapshot cannot be deleted
        if not self._can_delete_snapshot_in_objectstorage(snapshot):
            raise exception.SnapshotIsBusy(snapshot_name=snapshot['name'])

        @utils.synchronized(snapshot['volume_id'] + '-' + 'objectstorage')
        def do_delete_snapshot_in_objectstorage():
            snaper.delete_snapshot(snapshot, snapshot['name'])
        do_delete_snapshot_in_objectstorage()

    def _create_snapshot_in_image(self, snapshot):
        """Create a qcow2 snapshot"""
        volume_ref = self.db.volume_get(self._context, snapshot['volume_id'])
        if volume_ref['status'] in ['available', 'detaching', 'attaching']:
            vdisk.create_snapshot(volume_ref,
                                  volume_ref['name'],
                                  snapshot['name'])
        elif volume_ref['status'] == 'in-use':
            # NOTE(rongze) : forever not come here
            instance_uuid = volume_ref['instance_uuid']
            target_dev = volume_ref['mountpoint'].split('/')[-1]
            #get device for libvirt_qemu.qemuMonitorCommand function.
            #TODO we need to check instance status
            auth = [[libvirt.VIR_CRED_AUTHNAME, libvirt.VIR_CRED_NOECHOPROMPT],
                    'root',
                    None]
            conn = libvirt.openAuth('qemu:///system', auth, 0)
            dom = conn.lookupByUUIDString(instance_uuid)
            xml = dom.XMLDesc(0)
            doc = etree.fromstring(xml)
            ret = doc.findall('./devices/disk')
            find_node = None
            for node in ret:
                for child in node.getchildren():
                    if child.tag == 'target':
                        if child.get('dev') == target_dev:
                            find_node = node

            if not find_node:
                raise exception.Error("Island cannot find device!")

            device = None
            for child in find_node.getchildren():
                if child.tag == 'alias':
                    device = child.get('name')

            if not device:
                raise exception.Error("Island cannot find device name!")
            device = 'drive-' + device

            #use libvirt_qemu.qemuMonitorCommand to create snapshot
            cmd = 'live_snapshot_add  ' +\
                device + '  ' + snapshot['name'] +\
                '   qcow2'
            ret_string = libvirt_qemu.qemuMonitorCommand(dom, cmd, 1)
            ret = ret_string.strip()
            if ret:
                #create faild , TODO
                raise exception.Error("Island create snapshot faild")
        else:
            raise exception.Error('Invalid Operation')

    def _delete_snapshot_in_image(self, snapshot):
        """Deletes a qcow2 snapshot"""
        volume_ref = self.db.volume_get(self._context, snapshot['volume_id'])
        if volume_ref['status'] == 'available':
            vdisk.delete_snapshot(volume_ref,
                                  volume_ref['name'],
                                  snapshot['name'])
        elif volume_ref['status'] == 'in-use':
            instance_uuid = volume_ref['instance_uuid']
            target_dev = volume_ref['mountpoint'].split('/')[-1]

            #get device for libvirt_qemu.qemuMonitorCommand function.
            #TODO we need to check instance status
            auth = [[libvirt.VIR_CRED_AUTHNAME, libvirt.VIR_CRED_NOECHOPROMPT],
                    'root',
                    None]
            conn = libvirt.openAuth('qemu:///system', auth, 0)
            dom = conn.lookupByUUIDString(instance_uuid)
            xml = dom.XMLDesc(0)
            doc = etree.fromstring(xml)
            ret = doc.findall('./devices/disk')
            find_node = None
            for node in ret:
                for child in node.getchildren():
                    if child.tag == 'target':
                        if child.get('dev') == target_dev:
                            find_node = node

            if not find_node:
                raise exception.Error("Island cannot find device!")

            device = None
            for child in find_node.getchildren():
                if child.tag == 'alias':
                    device = child.get('name')

            if not device:
                raise exception.Error("Island cannot find device name!")
            device = 'drive-' + device

            #use libvirt_qemu.qemuMonitorCommand to delete snapshot
            cmd = 'live_snapshot_delete  ' +\
                device + '  ' + snapshot['name'] +\
                '   qcow2'
            ret_string = libvirt_qemu.qemuMonitorCommand(dom, cmd, 1)
            ret = ret_string.strip()
            if ret:
                #delete faild , TODO
                raise exception.Error("Island delete snapshot faild")

    def local_path(self, volume):
        return FLAGS.local_volume_directory + volume['name']
    
    def copy_image_to_volume(self, context, volume, image_service, image_id):
        image_utils.fetch_to_raw(context, image_service, image_id, self.local_path(volume))

    def ensure_export(self, context, volume):
        """Safely and Synchronously recreates an export for a logical volume"""
        pass

    def create_export(self, context, volume):
        """Exports the volume"""
        pass

    def remove_export(self, context, volume):
        """Removes an export for a logical volume"""
        pass

    def initialize_connection(self, volume, connector):
        #LOG.error(_("[minkyu]Volume host name is : %s"), volume['host'])
	if volume['host'] != connector['host']:
            return super(IslandDriver, self).initialize_connection(volume, connector)
        else:

        # find the snapshots
            snapshots = self.db.snapshot_get_all_for_volume_order_by_created(
                    self._context,
                    volume['id'])

            # create volume image
            vdisk.create_vdisk(volume,
                               volume['name'],
                               self._sizestr(volume['size']))

            if len(snapshots) != 0 or volume['snapshot_id']:
                snapshot_id = None
                if len(snapshots) == 0:
                    # new volume from snapshot
                    snapshot_id = volume['snapshot_id']
                else:
                    # old volume
                    snapshots.reverse()
                    snapshot_id = snapshots[0]['id']

                # create volume from snapshot
                snapshot_ref = self.db.snapshot_get(self._context, snapshot_id)
                if snapshot_ref['status'] != 'available':
                    raise exception.Error("last snapshot is not available")
                snaper.get_snapshot(snapshot_ref,
                                    snapshot_ref['name'],
                                    volume['name'])

                if len(snapshots) != 0:
                    # old volume
                    # NOTE(Rongze): we need make snapshot in image
                    @utils.synchronized(str(volume['id']) + '-' + 'image')
                    def do_create_snapshot_in_image():
                        self._create_snapshot_in_image(snapshot_ref)
                    do_create_snapshot_in_image()
            else:
                #new volume
                pass

            volume_path = self.local_path(volume)
            return {
                'driver_volume_type': 'localstorage',
                'data': {
                    'name': volume['name'],
                    'device_path': volume_path
                }
            }

    def terminate_connection(self, volume, connector, **kwargs):
        pass
    def _update_volume_stats(self):
        stats = {}
        backend_name = "LOCALSTORAGE"
        if self.configuration:
            backend_name = self.configuration.safe_get('volume_backend_name')
        stats["volume_backend_name"] = backend_name or 'LOCALSTORAGE'
        stats['vendor_name'] = 'Open Source'
        stats['driver_version'] = '1.0'
        stats['storage_protocol'] = 'localstorage'
        stats['total_capacity_gb'] = 0
        stats['free_capacity_gb'] = 0
        stats['reserved_percentage'] = self.configuration.reserved_percentage
        stats['QoS_support'] = False

        try:
            stdout, _err = self._execute('vgs', '--noheadings', '--nosuffix', '--unit=G', '-o', 'name,size,free',
        self.configuration.volume_group, run_as_root=True)
        except exception.ProcessExecutionError as exc:
            LOG.error(_("Error retrieving volume status: %s"), exc.stderr)
            stdout = False


        if stdout:
            volume = stdout.split()
            stats['total_capacity_gb'] = float(volume[1].replace(',', '.'))
            
            stats['free_capacity_gb'] = float(volume[2].replace(',', '.'))

        self._stats = stats

    def get_volume_stats(self, refresh=False):
        if refresh:
            self._update_volume_stats()
        return self._stats
